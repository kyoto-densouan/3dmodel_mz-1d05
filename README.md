# README #  
  
1/3スケールのMZ-1D05風小物のstlファイルです。  
スイッチ類、I/Oポート等は省略しています。  
  
組み合わせたファイルとパーツごとに配置したファイルの二種類があります。  
元ファイルはAUTODESK 123D DESIGNです。  

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_mz-1d05/raw/6b726abda6b8d2f9bf3069dd8c464279039fd7c4/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_mz-1d05/raw/6b726abda6b8d2f9bf3069dd8c464279039fd7c4/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_mz-1d05/raw/6b726abda6b8d2f9bf3069dd8c464279039fd7c4/ExampleImage.png)
